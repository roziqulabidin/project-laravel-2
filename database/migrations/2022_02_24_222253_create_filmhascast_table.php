<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmhascastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filmhascast', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('Film_id');
            $table->unsignedBigInteger('Cast_id');
            $table->timestamps();

            $table->foreign('Film_id')->references('id')->on('film');
            $table->foreign('Cast_id')->references('id')->on('cast');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filmhascast');
    }
}
